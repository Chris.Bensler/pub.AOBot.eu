global integer DEBUG    DEBUG = FALSE

global procedure set_debug(atom flag)
  DEBUG = (flag != FALSE)
end procedure

global procedure debug(sequence s)
  if DEBUG then puts(1,s) end if
end procedure

global procedure debugf(sequence s, sequence args)
  if DEBUG then printf(1,s,args) end if
end procedure
